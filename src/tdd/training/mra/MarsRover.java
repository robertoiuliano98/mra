package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	protected int planetX, planetY;
	protected List<String> planetObstacles;
	protected String position;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX-1;
		this.planetY = planetY-1;
		this.planetObstacles = planetObstacles;
		this.position = "(0,0,N)";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean flag = false;
		int i;
		for (i=0; i<this.planetObstacles.size(); i++) {
			String planetObstacle = this.planetObstacles.get(i);
			String coordinates = planetObstacle.substring(1, planetObstacle.length()-1);
			String[] coord = coordinates.split(",");
			int coordX = Integer.parseInt(coord[0]);
			int coordY = Integer.parseInt(coord[1]);
			
			if(coordX==x && coordY==y) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if(commandString.length() == 0) {
			return this.position;
		} else {
			// right = destra, left = sinistra
			String component[] = this.position.split(",");
			String x = (String) component[0].subSequence(1, component[0].length());
			String y = component[1];
			int coordX = Integer.parseInt(x);
			int coordY = Integer.parseInt(y);
			boolean flag = false;
			
			String direction = (String) this.position.split(",")[2].subSequence(0, 1);
			
			if(commandString.equalsIgnoreCase("r")) {
				if(direction.equalsIgnoreCase("N")) {
					direction = "E";
				} else if(direction.equalsIgnoreCase("E")) {
					direction = "S";
				} else if(direction.equalsIgnoreCase("S")) {
					direction = "W";
				} else if(direction.equalsIgnoreCase("W")) {
					direction = "N";
				}
				
				int n = this.position.split(",").length;
				String position1[] = this.position.split(",");
				position1[2] = direction + position1[2].substring(1, position1[2].length());
				
				String a = "";
				
				for (int j=0; j<n; j++) {
					a = a.concat(position1[j]);
					if(j<n-1) {
						a = a.concat(",");
					}
					this.position = a;
				}
				
				this.position = a;
				
			} else if (commandString.equalsIgnoreCase("l")) {
				if(direction.equalsIgnoreCase("N")) {
					direction = "W";
				} else if(direction.equalsIgnoreCase("W")) {
					direction = "S";
				} else if(direction.equalsIgnoreCase("S")) {
					direction = "E";
				} else if(direction.equalsIgnoreCase("E")) {
					direction = "N";
				}
				
				int n = this.position.split(",").length;
				//System.out.println(n);
				String position1[] = this.position.split(",");
				position1[2] = direction + position1[2].substring(1, position1[2].length());
				
				String a = "";
				
				for (int j=0; j<n; j++) {
					a = a.concat(position1[j]);
					if(j<n-1) {
						a = a.concat(",");
					}
					this.position = a;
				}
				
				//this.position = this.position.substring(0, 5).concat(direction+")");
				
			} else if (commandString.equalsIgnoreCase("f")) {
				//System.out.println("CoordX: " + x);
				//System.out.println("CoordY: " + y);
				if(direction.equalsIgnoreCase("N")) {
					if(Integer.parseInt(y) < this.planetY) {
						if(this.planetContainsObstacleAt(coordX, coordY+1)) {
							this.position.concat("("+coordX+","+ ++coordY +")");
							return this.position;
						} else {
							coordY = Integer.parseInt(y)+1;
						}
					} else {
						if(this.planetContainsObstacleAt(coordX, 0)) {
							this.position.concat("("+coordX+","+0+")");
							return this.position;
						} else {
							coordY = 0;
						}
					}
					flag = true;
				} else if(direction.equalsIgnoreCase("E")) {
					if(Integer.parseInt(x) < this.planetX) {
						if(this.planetContainsObstacleAt(coordX+1, coordY)) {
							//System.out.println("before" + this.position);
							this.position = this.position.concat("("+ ++coordX +","+coordY+")");
							//System.out.println("after" + this.position);
							return this.position;
						} else {
							coordX = Integer.parseInt(x)+1;
						}
					} else {
						if(this.planetContainsObstacleAt(0, coordY)) {
							this.position = this.position.concat("("+0+","+coordX+")");
							return this.position;
						} else {
							coordX = 0;
						}
					}
					flag = true;
				} else if(direction.equalsIgnoreCase("S")) {
					if(Integer.parseInt(y) > 0) {
						if(this.planetContainsObstacleAt(coordX, coordY-1)) {
							this.position = this.position.concat("("+coordX+","+ --coordY +")");
							return this.position;
						} else {
							coordY = Integer.parseInt(y)-1;
						}
					} else {
						if(this.planetContainsObstacleAt(coordX, this.planetY)) {
							this.position = this.position.concat("("+coordX+","+this.planetY+")");
							return this.position;
						} else {
							coordY = this.planetY;
						}
					}
					flag = true;
					
				} else if(direction.equalsIgnoreCase("W")) {
					if(Integer.parseInt(x) > 0) {
						if(this.planetContainsObstacleAt(coordX-1, coordY)) {
							this.position = this.position.concat("("+ --coordX+","+coordY+")");
							return this.position;
						} else {
							coordX = Integer.parseInt(x)-1;
						}
					} else {
						if(this.planetContainsObstacleAt(this.planetX, coordY)) {
							this.position = this.position.concat("("+this.planetX+","+coordY+")");
							return this.position;
						} else {
							coordX = this.planetX;
						}
					}
					flag = true;
				}
				
				if(flag == true) {
					
					
					int n = this.position.split(",").length;
					//System.out.println(n);
					String position1[] = this.position.split(",");
					position1[0] = "(" + String.valueOf(coordX);
					position1[1] = String.valueOf(coordY);
					
					String a = "";
					
					for (int j=0; j<n; j++) {
						a = a.concat(position1[j]);
						if(j<n-1) {
							a = a.concat(",");
						}
						this.position = a;
					}
					
					
					//this.position = "(" + String.valueOf(coordX) + "," + String.valueOf(coordY) + "," + direction + ")";
					flag = false;
				}
				
			} else if (commandString.equalsIgnoreCase("b")) {
				//System.out.println("CoordX: " + x);
				//System.out.println("CoordY: " + y);
				if(direction.equalsIgnoreCase("N")) {
					if(Integer.parseInt(y) > 0) {
						if(this.planetContainsObstacleAt(coordX, coordY-1)) {
							this.position = this.position.concat("("+coordX+","+ --coordY +")");
							return this.position;
						} else {
							coordY = Integer.parseInt(y)-1;
						}
					} else {	
						if(this.planetContainsObstacleAt(coordX, this.planetY)) {
							this.position = this.position.concat("("+coordX+","+ this.planetY +")");
							return this.position;
						} else {
							coordY = this.planetY;
						}
					}
					flag = true;
				} else if(direction.equalsIgnoreCase("E")) {
					if(Integer.parseInt(x) > 0) {
						if(this.planetContainsObstacleAt(coordX-1, coordY)) {
							this.position = this.position.concat("("+ --coordX + ","+ coordY +")");
							return this.position;
						} else {
							coordX = Integer.parseInt(x)-1;
						}						
					} else {
						if(this.planetContainsObstacleAt(this.planetX, coordY)) {
							this.position = this.position.concat("("+this.planetY+","+ coordY +")");
							return this.position;
						} else {
							coordY = this.planetY;
						}
					}
					flag = true;
				} else if(direction.equalsIgnoreCase("S")) {
					if(Integer.parseInt(y) < this.planetY) {
						if(this.planetContainsObstacleAt(coordX, coordY+1)) {
							this.position = this.position.concat("("+ coordX + ","+ ++coordY +")");
							return this.position;
						} else {
							coordY = Integer.parseInt(y)+1;
						}				
					} else {	
						if(this.planetContainsObstacleAt(coordX, 0)) {
							this.position = this.position.concat("("+coordX+","+ 0 +")");
							return this.position;
						} else {
							coordY = 0;
						}
					}
					flag = true;
				} else if(direction.equalsIgnoreCase("W")) {
					if(Integer.parseInt(x) < this.planetX) {	
						if(this.planetContainsObstacleAt(coordX-1, coordY)) {
							this.position = this.position.concat("("+ --coordX + ","+ coordY +")");
							return this.position;
						} else {
							coordX = Integer.parseInt(x)-1;
						}		
					} else {
						
						if(this.planetContainsObstacleAt(0, coordY)) {
							this.position = this.position.concat("("+0+","+ coordY +")");
							return this.position;
						} else {
							coordX = 0;
						}
					}
					flag = true;
				}
				
				if(flag == true) {
					
					int n = this.position.split(",").length;
					//System.out.println(n);
					String position1[] = this.position.split(",");
					position1[0] = "(" + String.valueOf(coordX);
					position1[1] = String.valueOf(coordY);
					
					String a = "";
					
					for (int j=0; j<n; j++) {
						a = a.concat(position1[j]);
						if(j<n-1) {
							a = a.concat(",");
						}
						this.position = a;
					}
					
					//this.position = "(" + String.valueOf(coordX) + "," + String.valueOf(coordY) + "," + direction + ")";
					flag = false;
				}
				
			}
			//System.out.println("New position: " + this.position);
			return this.position;
		}
		
	}

}
