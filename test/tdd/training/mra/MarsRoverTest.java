package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	//User story 1 - Planet
	public void testPlanet() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//System.out.println(rover.planetContainsObstacleAt(7, 8));
		//assertEquals(true, rover.planetContainsObstacleAt(7, 8));

		
	}
	
	@Test
	//User story 2 - Landing
	public void testLanding() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//System.out.println(rover.executeCommand(""));
		//assertEquals(rover.executeCommand(""), "(0,0,N)");

		
	}
	
	@Test
	//User story 3 - Turning
	public void testTurning() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//System.out.println(rover.executeCommand("r"));
		//assertEquals(rover.executeCommand("r"), "(0,0,E)");

		
	}
	
	@Test
	//User story 4 - Forward
	public void testForward() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//System.out.println(rover.executeCommand("f"));
		//assertEquals(rover.executeCommand("f"), "(0,1,N)");

		
	}
	
	@Test
	//User story 5 - Backward
	public void testBackward() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//System.out.println(rover.executeCommand("b"));
		//assertEquals(rover.executeCommand("b"), "(0,0,N)");

		
	}
	
	@Test
	//User story 6 - Combined
	public void testCombined() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String newPosition = null;
		
		/*
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		
		assertEquals(newPosition, "(2,2,E)");
		*/

		
	}
	
	@Test
	//User story 7 - Wrapping
	public void testWrapping() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		//rover.executeCommand("b");
		
		//assertEquals(newPosition, "(2,2,E)");

		
	}
	
	@Test
	//User story 8 - singleObstacle
	public void testSingleObstacle() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		/*String newPosition = null;
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		
		System.out.println(newPosition);
		
		assertEquals(newPosition, "(1,2,E)(2,2)");*/

		
	}
	
	@Test
	//User story 9 - multipleObstacle
	public void testMultipleObstacle() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String newPosition = null;
		/*newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("l");
		newPosition = rover.executeCommand("f");
		
		
		
		System.out.println(newPosition);
		
		assertEquals(newPosition, "(1,1,E)(2,2)(2,2)(2,1)");*/

		
	}
	
	@Test
	//User story 10 - wrappingAndObstacles
	public void testwrappingAndObstacles() throws MarsRoverException {
			
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
			
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
						
		//System.out.println(rover.executeCommand("b"));
			
		//assertEquals(rover.executeCommand("b"), "(0,0,N)(1,9)");

			
	}
	
	@Test
	//User story 11 - tour
	public void testTour() throws MarsRoverException {
			
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
			
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
						
		
			
		String newPosition = null;
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("b");
		newPosition = rover.executeCommand("b");
		newPosition = rover.executeCommand("b");
		newPosition = rover.executeCommand("l");
		newPosition = rover.executeCommand("l");
		newPosition = rover.executeCommand("l");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("f");
		newPosition = rover.executeCommand("r");
		newPosition = rover.executeCommand("b");
		newPosition = rover.executeCommand("b");
		newPosition = rover.executeCommand("l");
		
		
		
		System.out.println(newPosition);
		
		assertEquals(newPosition, "(0,0,N)(2,2)(2,2)(0,5)(5,0)");


			
	}
	
	

}
